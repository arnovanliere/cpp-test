#ifndef TEST_H
#define TEST_H

#include <chrono>
#include <functional>
#include "test_exception.h"

class test {
public:
    /**
     * Default constructor.
     */
    test();

    /**
     * Test if two values are the same.
     * @param expected The expected value.
     * @param actual   The actual value.
     */
    template<typename T>
    void assert_eq(T expected, T actual) {
        if (expected != actual) {
            throw test_exception("Expected != Actual");
        }
    }

    /**
     * Test if two values are the same.
     * @param expected The expected value.
     * @param actual   The actual value.
     * @param msg      The message to print if check fails.
     */
    template<typename T>
    void assert_eq(T expected, T actual, const std::string &msg) {
        if (expected != actual) {
            throw test_exception("Expected != Actual\nMessage:\n" + msg);
        }
    }

    /**
     * Run a lambda.
     * @param name A description of the function.
     * @param fn   The function to run.
     */
    void run(const std::string &name, const std::function<void(test *t)> &fn);

    /**
     * Print stats and exit with exit-code.
     */
    void end();

private:
    // When test-suite started
    std::chrono::time_point<std::chrono::system_clock> start;

    // How many tests passed
    int passed;

    // How many tests failed
    int failed;
};

#endif
