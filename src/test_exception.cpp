#include "test_exception.h"

test_exception::test_exception(const std::string &reason) {
    this->reason = reason;
}

const char *test_exception::what() const noexcept {
    return reason.c_str();
}
