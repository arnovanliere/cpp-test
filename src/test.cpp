#include "test.h"
#include "test_exception.h"
#include <iostream>

test::test() : passed(0), failed(0), start(std::chrono::high_resolution_clock::now()) {}

void test::run(const std::string &name, const std::function<void(test *)> &fn) {
    try {
        fn(this);
        std::cout << "✓ " << name << " passed" << std::endl;
        passed++;
    } catch (test_exception &e) {
        std::cout << "⨯ " << name << " failed" << std::endl
                  << e.what() << std::endl;
        failed++;
    }
}

void test::end() {
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    int total = passed + failed;
    std::cout << passed << "/" << total << " tests passed"
              << std::endl << failed << "/" << total << " tests failed"
              << std::endl << "Duration: " << duration << "ms";
    if (failed != 0) exit(1);
    else exit(0);
}