#ifndef TESTS_EXCEPTION_H
#define TESTS_EXCEPTION_H

#include <exception>
#include <string>

class test_exception : std::exception {
public:
    /**
     * Default constructor.
     * @param reason The reason this exception is thrown.
     */
    explicit test_exception(const std::string &reason);

    /**
     * Retrieve the reason for this exception.
     * @return The reason.
     */
    [[nodiscard]] const char *what() const noexcept override;

private:
    // The reason this exception is thrown
    std::string reason;
};

#endif
